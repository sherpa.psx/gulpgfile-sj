var gulp            = require('gulp'),
    config          = require('../config'),
    data            = require('gulp-data'),
    fs              = require('fs'),
    twig            = require('gulp-twig'),
    browserSync     = require('browser-sync');

var json = JSON.parse(fs.readFileSync(config.templates.src + 'data.json'))

gulp.task('templates', function(){
  return gulp.src(config.templates.src + '**/*.twig')
    .pipe(twig({ data: json }))
    .pipe(gulp.dest(config.templates.dist))
    .on('end', function(){
      browserSync.reload()
    });
});
