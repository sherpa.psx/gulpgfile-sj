var gulp          = require('gulp'),
    config        = require('../config'),
    sass          = require('gulp-sass'),
    postcss       = require('gulp-postcss'),
    autoprefixer  = require('autoprefixer'),
    cssnano       = require('cssnano'),
    pxtorem       = require('postcss-pxtorem'),
    cssNesting    = require('postcss-nesting'),
    postFlexbox   = require('postcss-flexbox'),
    postInlineSvg = require('postcss-inline-svg');


var pxremOptions = {
  rootValue: 16,
  unitPrecision: 5,
  propList: ['margin','padding','border-width',],
  selectorBlackList: [],
  replace: true,
  mediaQuery: false,
  minPixelValue: 0
};

var plugins = [
  autoprefixer({browsers: ['last 1 version']}),
  cssnano(),
  pxtorem(pxremOptions),
  cssNesting(),
  postFlexbox()
];

gulp.task('styles:build', function() {
  gulp.src(config.styles.src + 'style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(plugins))
    .pipe(gulp.dest(config.styles.dist))
});
