var gulp            = require('gulp'),
    config          = require('../config'),
    concat          = require('gulp-concat'),
    uglify          = require('gulp-uglify');


gulp.task('scripts', function() {
    return gulp.src(config.scripts.src + '**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest(config.scripts.dist));
});

