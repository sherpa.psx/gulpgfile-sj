var gulp          = require('gulp'),
    config        = require('../config'),
    sass          = require('gulp-sass'),
    sourcemaps    = require('gulp-sourcemaps'),
    postcss       = require('gulp-postcss'),
    browserSync   = require('browser-sync');

gulp.task('styles', function(){
  gulp.src(config.styles.src + 'style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(config.styles.dist))
    .pipe(browserSync.stream());
});
