/**
 * Created by lukaswerner on 21.07.17.
 */

$(document).ready(function () {
    svg4everybody({
        polyfill: true
    });

    $('.settings-nav').horizonSwiper({
        animationSpeed: 200
    });

    
    
    //Sticky main menu
    $(".sj-menu-main").stick_in_parent({

    });


    //Sticky candidate bar
    $(".sj-candidate-bar").stick_in_parent({
        offset_top: 63,
    }).on("sticky_kit:stick", function(e) {

    });

    $('.sj-showmore-icon').click(function () {
        $('.menu-more-content').toggle();
        $(".sj-menu-main").toggleClass('expanded')
    })
});
